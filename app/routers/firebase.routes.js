const express = require('express');
var router = express.Router();

const firebase = require('../controllers/firebase.controller');

router.post('/', firebase.control);

module.exports=router